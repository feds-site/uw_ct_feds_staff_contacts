<?php

/**
 * @file
 * uw_ct_feds_staff_contacts.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function uw_ct_feds_staff_contacts_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'feds_contacts';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Feds Contacts';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Staff Directory';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['reset_button'] = TRUE;
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['row_class'] = 'feds-contact-container';
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['inline'] = array(
    'field_feds_first_name_1' => 'field_feds_first_name_1',
    'field_feds_last_name' => 'field_feds_last_name',
    'field_position' => 'field_position',
    'field_feds_department' => 'field_feds_department',
    'field_phone_number' => 'field_phone_number',
    'field_email' => 'field_email',
    'field_office_location' => 'field_office_location',
    'body' => 'body',
  );
  $handler->display->display_options['row_options']['hide_empty'] = TRUE;
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['nid']['alter']['text'] = '<span class="content-id" id="[nid]">[nid]</span>';
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['nid']['element_default_classes'] = FALSE;
  /* Field: Content: First Name */
  $handler->display->display_options['fields']['field_feds_first_name_1']['id'] = 'field_feds_first_name_1';
  $handler->display->display_options['fields']['field_feds_first_name_1']['table'] = 'field_data_field_feds_first_name';
  $handler->display->display_options['fields']['field_feds_first_name_1']['field'] = 'field_feds_first_name';
  $handler->display->display_options['fields']['field_feds_first_name_1']['label'] = '';
  $handler->display->display_options['fields']['field_feds_first_name_1']['element_type'] = 'h3';
  $handler->display->display_options['fields']['field_feds_first_name_1']['element_class'] = 'contact-firstname';
  $handler->display->display_options['fields']['field_feds_first_name_1']['element_label_colon'] = FALSE;
  /* Field: Content: Last Name */
  $handler->display->display_options['fields']['field_feds_last_name']['id'] = 'field_feds_last_name';
  $handler->display->display_options['fields']['field_feds_last_name']['table'] = 'field_data_field_feds_last_name';
  $handler->display->display_options['fields']['field_feds_last_name']['field'] = 'field_feds_last_name';
  $handler->display->display_options['fields']['field_feds_last_name']['label'] = '';
  $handler->display->display_options['fields']['field_feds_last_name']['element_type'] = 'h3';
  $handler->display->display_options['fields']['field_feds_last_name']['element_class'] = 'contact-lastname';
  $handler->display->display_options['fields']['field_feds_last_name']['element_label_colon'] = FALSE;
  /* Field: Content: Position or Title */
  $handler->display->display_options['fields']['field_position']['id'] = 'field_position';
  $handler->display->display_options['fields']['field_position']['table'] = 'field_data_field_position';
  $handler->display->display_options['fields']['field_position']['field'] = 'field_position';
  $handler->display->display_options['fields']['field_position']['label'] = '';
  $handler->display->display_options['fields']['field_position']['alter']['nl2br'] = TRUE;
  $handler->display->display_options['fields']['field_position']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_position']['element_wrapper_type'] = 'div';
  $handler->display->display_options['fields']['field_position']['hide_alter_empty'] = FALSE;
  /* Field: Content: Department */
  $handler->display->display_options['fields']['field_feds_department']['id'] = 'field_feds_department';
  $handler->display->display_options['fields']['field_feds_department']['table'] = 'field_data_field_feds_department';
  $handler->display->display_options['fields']['field_feds_department']['field'] = 'field_feds_department';
  $handler->display->display_options['fields']['field_feds_department']['label'] = '';
  $handler->display->display_options['fields']['field_feds_department']['alter']['nl2br'] = TRUE;
  $handler->display->display_options['fields']['field_feds_department']['element_class'] = 'contact-position';
  $handler->display->display_options['fields']['field_feds_department']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_feds_department']['element_wrapper_type'] = 'div';
  $handler->display->display_options['fields']['field_feds_department']['type'] = 'taxonomy_term_reference_plain';
  /* Field: Content: Phone Number */
  $handler->display->display_options['fields']['field_phone_number']['id'] = 'field_phone_number';
  $handler->display->display_options['fields']['field_phone_number']['table'] = 'field_data_field_phone_number';
  $handler->display->display_options['fields']['field_phone_number']['field'] = 'field_phone_number';
  $handler->display->display_options['fields']['field_phone_number']['label'] = '';
  $handler->display->display_options['fields']['field_phone_number']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_phone_number']['element_wrapper_type'] = 'div';
  /* Field: Content: Email */
  $handler->display->display_options['fields']['field_email']['id'] = 'field_email';
  $handler->display->display_options['fields']['field_email']['table'] = 'field_data_field_email';
  $handler->display->display_options['fields']['field_email']['field'] = 'field_email';
  $handler->display->display_options['fields']['field_email']['label'] = '';
  $handler->display->display_options['fields']['field_email']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_email']['element_wrapper_type'] = 'div';
  /* Field: Content: Office Location */
  $handler->display->display_options['fields']['field_feds_office_location']['id'] = 'field_feds_office_location';
  $handler->display->display_options['fields']['field_feds_office_location']['table'] = 'field_data_field_feds_office_location';
  $handler->display->display_options['fields']['field_feds_office_location']['field'] = 'field_feds_office_location';
  $handler->display->display_options['fields']['field_feds_office_location']['label'] = 'Office';
  $handler->display->display_options['fields']['field_feds_office_location']['element_class'] = 'contact-office';
  $handler->display->display_options['fields']['field_feds_office_location']['element_wrapper_type'] = 'div';
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['element_wrapper_type'] = 'div';
  /* Sort criterion: Content: Department (field_feds_department) */
  $handler->display->display_options['sorts']['field_feds_department_tid_1']['id'] = 'field_feds_department_tid_1';
  $handler->display->display_options['sorts']['field_feds_department_tid_1']['table'] = 'field_data_field_feds_department';
  $handler->display->display_options['sorts']['field_feds_department_tid_1']['field'] = 'field_feds_department_tid';
  /* Sort criterion: Content: First Name (field_feds_first_name:format) */
  $handler->display->display_options['sorts']['field_feds_first_name_format']['id'] = 'field_feds_first_name_format';
  $handler->display->display_options['sorts']['field_feds_first_name_format']['table'] = 'field_data_field_feds_first_name';
  $handler->display->display_options['sorts']['field_feds_first_name_format']['field'] = 'field_feds_first_name_format';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'feds_contacts' => 'feds_contacts',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;

  /* Display: Feds Staff Directory Page */
  $handler = $view->new_display('page', 'Feds Staff Directory Page', 'page');
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: Content: First Name (field_feds_first_name) */
  $handler->display->display_options['sorts']['field_feds_first_name_value']['id'] = 'field_feds_first_name_value';
  $handler->display->display_options['sorts']['field_feds_first_name_value']['table'] = 'field_data_field_feds_first_name';
  $handler->display->display_options['sorts']['field_feds_first_name_value']['field'] = 'field_feds_first_name_value';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'feds_contacts' => 'feds_contacts',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: Department (field_feds_department) */
  $handler->display->display_options['filters']['field_feds_department_tid']['id'] = 'field_feds_department_tid';
  $handler->display->display_options['filters']['field_feds_department_tid']['table'] = 'field_data_field_feds_department';
  $handler->display->display_options['filters']['field_feds_department_tid']['field'] = 'field_feds_department_tid';
  $handler->display->display_options['filters']['field_feds_department_tid']['group'] = 1;
  $handler->display->display_options['filters']['field_feds_department_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_feds_department_tid']['expose']['operator_id'] = 'field_feds_department_tid_op';
  $handler->display->display_options['filters']['field_feds_department_tid']['expose']['label'] = 'Department';
  $handler->display->display_options['filters']['field_feds_department_tid']['expose']['operator'] = 'field_feds_department_tid_op';
  $handler->display->display_options['filters']['field_feds_department_tid']['expose']['identifier'] = 'field_feds_department_tid';
  $handler->display->display_options['filters']['field_feds_department_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    21 => 0,
    16 => 0,
    19 => 0,
    13 => 0,
    14 => 0,
    15 => 0,
    3 => 0,
    6 => 0,
    12 => 0,
    5 => 0,
    4 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    11 => 0,
    10 => 0,
    18 => 0,
    20 => 0,
    17 => 0,
  );
  $handler->display->display_options['filters']['field_feds_department_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_feds_department_tid']['vocabulary'] = 'feds_departments';
  $handler->display->display_options['filters']['field_feds_department_tid']['hierarchy'] = 1;
  /* Filter criterion: Content: First Name (field_feds_first_name) */
  $handler->display->display_options['filters']['field_feds_first_name_value']['id'] = 'field_feds_first_name_value';
  $handler->display->display_options['filters']['field_feds_first_name_value']['table'] = 'field_data_field_feds_first_name';
  $handler->display->display_options['filters']['field_feds_first_name_value']['field'] = 'field_feds_first_name_value';
  $handler->display->display_options['filters']['field_feds_first_name_value']['operator'] = 'contains';
  $handler->display->display_options['filters']['field_feds_first_name_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_feds_first_name_value']['expose']['operator_id'] = 'field_feds_first_name_value_op';
  $handler->display->display_options['filters']['field_feds_first_name_value']['expose']['label'] = 'First Name';
  $handler->display->display_options['filters']['field_feds_first_name_value']['expose']['operator'] = 'field_feds_first_name_value_op';
  $handler->display->display_options['filters']['field_feds_first_name_value']['expose']['identifier'] = 'field_feds_first_name_value';
  $handler->display->display_options['filters']['field_feds_first_name_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    21 => 0,
    16 => 0,
    19 => 0,
    13 => 0,
    14 => 0,
    15 => 0,
    3 => 0,
    6 => 0,
    12 => 0,
    5 => 0,
    4 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    11 => 0,
    10 => 0,
    18 => 0,
    20 => 0,
    17 => 0,
  );
  /* Filter criterion: Content: Last Name (field_feds_last_name) */
  $handler->display->display_options['filters']['field_feds_last_name_value']['id'] = 'field_feds_last_name_value';
  $handler->display->display_options['filters']['field_feds_last_name_value']['table'] = 'field_data_field_feds_last_name';
  $handler->display->display_options['filters']['field_feds_last_name_value']['field'] = 'field_feds_last_name_value';
  $handler->display->display_options['filters']['field_feds_last_name_value']['operator'] = 'contains';
  $handler->display->display_options['filters']['field_feds_last_name_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_feds_last_name_value']['expose']['operator_id'] = 'field_feds_last_name_value_op';
  $handler->display->display_options['filters']['field_feds_last_name_value']['expose']['label'] = 'Last Name';
  $handler->display->display_options['filters']['field_feds_last_name_value']['expose']['operator'] = 'field_feds_last_name_value_op';
  $handler->display->display_options['filters']['field_feds_last_name_value']['expose']['identifier'] = 'field_feds_last_name_value';
  $handler->display->display_options['filters']['field_feds_last_name_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    21 => 0,
    16 => 0,
    19 => 0,
    13 => 0,
    14 => 0,
    15 => 0,
    3 => 0,
    6 => 0,
    12 => 0,
    5 => 0,
    4 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    11 => 0,
    10 => 0,
    18 => 0,
    20 => 0,
    17 => 0,
  );
  $handler->display->display_options['path'] = 'feds_contact/staff-directory';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Staff Directory';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  /* Display: Block Marketing */
  $handler = $view->new_display('block', 'Block Marketing', 'block');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Marketing & Communications';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'feds_contacts' => 'feds_contacts',
  );

  /* Display: Block IT */
  $handler = $view->new_display('block', 'Block IT', 'block_1');
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'feds_contacts' => 'feds_contacts',
  );
  /* Filter criterion: Broken/missing handler */
  $handler->display->display_options['filters']['field_department_value_1']['id'] = 'field_department_value_1';
  $handler->display->display_options['filters']['field_department_value_1']['table'] = 'field_data_field_department';
  $handler->display->display_options['filters']['field_department_value_1']['field'] = 'field_department_value';
  $handler->display->display_options['filters']['field_department_value_1']['value'] = 'IT';
  $translatables['feds_contacts'] = array(
    t('Master'),
    t('Staff Directory'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('<span class="content-id" id="[nid]">[nid]</span>'),
    t('Office'),
    t('Feds Staff Directory Page'),
    t('Department'),
    t('First Name'),
    t('Last Name'),
    t('Block Marketing'),
    t('Marketing & Communications'),
    t('Block IT'),
  );
  $export['feds_contacts'] = $view;

  return $export;
}
