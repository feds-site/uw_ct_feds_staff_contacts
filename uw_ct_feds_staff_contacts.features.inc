<?php

/**
 * @file
 * uw_ct_feds_staff_contacts.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_feds_staff_contacts_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_ct_feds_staff_contacts_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function uw_ct_feds_staff_contacts_node_info() {
  $items = array(
    'feds_contacts' => array(
      'name' => t('Feds contacts'),
      'base' => 'node_content',
      'description' => t('Feds staff contact details.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
