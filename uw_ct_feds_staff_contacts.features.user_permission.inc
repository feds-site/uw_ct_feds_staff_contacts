<?php

/**
 * @file
 * uw_ct_feds_staff_contacts.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_feds_staff_contacts_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create feds_contacts content'.
  $permissions['create feds_contacts content'] = array(
    'name' => 'create feds_contacts content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any feds_contacts content'.
  $permissions['delete any feds_contacts content'] = array(
    'name' => 'delete any feds_contacts content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own feds_contacts content'.
  $permissions['delete own feds_contacts content'] = array(
    'name' => 'delete own feds_contacts content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete terms in feds_departments'.
  $permissions['delete terms in feds_departments'] = array(
    'name' => 'delete terms in feds_departments',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit any feds_contacts content'.
  $permissions['edit any feds_contacts content'] = array(
    'name' => 'edit any feds_contacts content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own feds_contacts content'.
  $permissions['edit own feds_contacts content'] = array(
    'name' => 'edit own feds_contacts content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'enter feds_contacts revision log entry'.
  $permissions['enter feds_contacts revision log entry'] = array(
    'name' => 'enter feds_contacts revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_contacts authored by option'.
  $permissions['override feds_contacts authored by option'] = array(
    'name' => 'override feds_contacts authored by option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_contacts authored on option'.
  $permissions['override feds_contacts authored on option'] = array(
    'name' => 'override feds_contacts authored on option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_contacts promote to front page option'.
  $permissions['override feds_contacts promote to front page option'] = array(
    'name' => 'override feds_contacts promote to front page option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_contacts published option'.
  $permissions['override feds_contacts published option'] = array(
    'name' => 'override feds_contacts published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_contacts revision option'.
  $permissions['override feds_contacts revision option'] = array(
    'name' => 'override feds_contacts revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_contacts sticky option'.
  $permissions['override feds_contacts sticky option'] = array(
    'name' => 'override feds_contacts sticky option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'search feds_contacts content'.
  $permissions['search feds_contacts content'] = array(
    'name' => 'search feds_contacts content',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'search_config',
  );

  return $permissions;
}
